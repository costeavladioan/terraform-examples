terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }

  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/41568033/terraform/state/stage_echo"
    lock_address   = "https://gitlab.com/api/v4/projects/41568033/terraform/state/stage_echo/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/41568033/terraform/state/stage_echo/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
  }
}

resource "random_id" "value" {
  byte_length = 8
}

module "echo" {
  source = "../../modules/echo"
}