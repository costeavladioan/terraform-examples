resource "aws_s3_bucket" "bucket" {
  bucket = var.bucket_name

  tags = {
    Environment = var.env
    Trigger = "Sentinel tag used as a trigger for terraform changes when experimenting with the pipeline"
    TriggerVersion = "16"
  }
}

resource "aws_s3_bucket_acl" "bucket_acl" {
  bucket = aws_s3_bucket.bucket.id
  acl    = "private"
}
