terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }

  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/41568033/terraform/state/prod_s3_bucket"
    lock_address   = "https://gitlab.com/api/v4/projects/41568033/terraform/state/prod_s3_bucket/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/41568033/terraform/state/prod_s3_bucket/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
  }
}

provider "aws" {
  region = "eu-central-1"
}

module "s3_bucket" {
  source = "../../modules/s3"
  bucket_name = "prod.terraform-examples"
  env = "prod"
}